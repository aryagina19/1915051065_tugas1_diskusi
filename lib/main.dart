import 'package:flutter/material.dart';

void main() {
  runApp(
      MaterialApp(
          home: Scaffold(
            backgroundColor: Colors.grey,
            appBar: AppBar(
              title: Text('Logo UNDIKSHA'),
              backgroundColor: Colors.blueAccent,
              leading: new IconButton(
                  icon: new Icon(Icons.apps, color: Colors.orangeAccent),onPressed: null),
              actions: <Widget>[
                new IconButton(
                    icon: new Icon(Icons.app_registration, color: Colors.orangeAccent),onPressed: null),
                new IconButton(
                    icon: new Icon(Icons.account_circle, color: Colors.orangeAccent),onPressed: null),
              ],
            ),
            body: Center(
              child: Image(
                image: NetworkImage(
                    'http://3.bp.blogspot.com/-gr97pv_oSqc/Uq8k6L8Z2RI/AAAAAAAADlY/1E3SoCkXnLk/s1600/Undiksha+Logo-01.png'),
              ),
            ),
          )
      )
  );
}